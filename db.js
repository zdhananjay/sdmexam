const db =require('mysql2')

const pool = db.createConnection(
    {
        host:'localhost',
        user:'root',
        password:'manager',
        waitForConnections: true,
        connectionLimit:10,
        database:'advjava2'
    });


module.exports={
    pool,
}