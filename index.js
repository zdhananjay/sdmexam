const express = require('express')
const mysql = require('mysql')
const cors = require('cors')

const app = express();
app.use(cors('*'))
app.use(express.json());

const connection = mysql.createConnection(
    {
        host:'localhost',
        user:'root',
        password:'manager',
        waitForConnections: true,
        connectionLimit:10,
        database:'advjava2'
    });


    connection.connect((err) =>
    {
        if(!err)
        {
            console.log("Datbase connection  is esatblished");
        }
        else
        {
            console.log("Database connection fails");
        }
    })

//get method
    app.get('/users', (req, res)=>
    {
        const query = "SELECT * FROM books";

        connection.query(query, (err, result)=>
        {
            if(result != null)
            {
                res.send(result);
            }
            else
            {
                res.send(err);
            }
        })
    })

    // get method for particular employee
    app.get('/users/:id', (req, res)=>
    {
        const query = `SELECT * FROM books WHERE bno = ${req.params.id}`;

        connection.query(query, (err, result)=>
        {
            if(result != null)
            {
                res.send(result);
            }
            else
            {
                res.send(err);
            }
        })
    })

    //insert employee details
    app.post('/users', (req, res)=>
    {
        const query = `INSERT INTO books VALUES 
        (${req.body.name}, '${req.body.password}', ${req.body.email},${req.body.city},
        ${req.body.state})`;

        connection.query(query, (err, result)=>
        {
            if(err == null)
            {
                res.send("Books details inserted successfully");
            }
            else
            {
                res.send(err);
            }
        })
    })

    //update employee details
    app.put('/users/:id', (req, res)=>
    {
        const query = `UPDATE books set book_name = '${req.body.book_name}',author='${req.body.author}', bprice = ${req.body.bprice} WHERE bno = ${req.params.id}`;

        connection.query(query, (err, result)=>
        {
            if(err == null)
            {
                res.send("Book details updated successfully");
            }
            else
            {
                res.send(err);
            }
        })
    })

    //removed employee details
    app.delete('/users/:id', (req, res)=>
    {
        const query = `DELETE FROM books WHERE bno = ${req.params.id}`;

        connection.query(query, (err, result)=>
        {
            if(err == null)
            {
                res.send("Book details removed successfully");
            }
            else
            {
                res.send(err);
            }
        })
    })

    app.listen(3000, ()=>
    {
        console.log("Server is started at port 3000");
    })